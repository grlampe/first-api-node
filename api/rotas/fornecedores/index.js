//Configurando as Rotas
const roteador = require('express').Router();

//Utilizando o "Repository"
const TabelaFornecedor = require('./tabelaFornecedor')

//Importando a Classe
const Fornecedor = require('./fornecedor');
const { response } = require('express');
const { json } = require('express/lib/response');

//GET
roteador.get('/', async (req, res) => {
    const result = await TabelaFornecedor.listar()
    res.send(
        JSON.stringify(result)
    )
});

//POST
roteador.post('/', async (req, res) => {
    const dadosRecebidos = req.body;
    const fornecedor = new Fornecedor(dadosRecebidos);
    await fornecedor.criar()

    res.send(
        JSON.stringify(fornecedor)
    )
});

//GET
roteador.get('/:idFornecedor', async (req, res) => {

    try {
        const id = req.params.idFornecedor;
        const fornecedor = new Fornecedor({id: id});
        await fornecedor.carregar()

        res.send(
            JSON.stringify(fornecedor)
        )    
    } catch (error) {
        res.send(
            JSON.stringify({
                mensagem: error.message
            })
        )       
    }

});

//PUT
roteador.put('/:idFornecedor', async (req, res) => {
    try {
        const id = req.params.idFornecedor;
        const dadosRecebidos = req.body;
        const dados = Object.assign({}, dadosRecebidos, { id: id})
        const fornecedor = new Fornecedor(dados);

        await fornecedor.atualizar()

        res.end('Atualizado')       
    } catch (error) {
        res.send(
            JSON.stringify({
                mensagem: error.message
            })
        )     
    }
});

module.exports = roteador;