//Importando a Lib para criar um servidor.
const express = require('express');
//Inicializando a Instancia
const app = express();
//Importando a Lib que diz que vamos trabalhar com formato JSON
const bodyParser = require('body-parser');
//Importando os Dados de Configuração da API
const config = require('config');


//Dizendo para o Servidor que vamos trabalhar com formato JSON
app.use(bodyParser.json());

//Configurando as Rotas
const roteador = require('./rotas/fornecedores')
app.use('/api/fornecedores', roteador);

//Inicializando o Servidor
app.listen(config.get('api.porta'), () => console.log('A API está funcionando!!'))