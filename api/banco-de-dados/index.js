//Importando Lib para Banco de dados
const Sequelize = require('sequelize');

//Importando os Dados de Configuração do Banco de Dados
const config = require('config');

//Criando o Objeto de Instancia
const instancia = new Sequelize(
    //NomeDoBanco
    config.get('mysql.banco-de-dados'),
    //Usuario
    config.get('mysql.usuario'),
    //Senha
    config.get('mysql.senha'),
    //Propriedades
    {
        host: config.get('mysql.host'),
        dialect: 'mysql'
    }
);

//Exportando o Objeto de Instancia
module.exports = instancia;