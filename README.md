Este Projeto é uma parte dos Estudos da ALURA (crie uma API REST) em NodeJS.

Este projeto visa criar uma API para um PETSHOP

Está API vai realizar:

 - Insert de um Fornecedor 
 - Update de um Fornecedor 
 - Delete de um Fornecedor 
 - Listar Fornecedores 
 - Obter Detalhes sobre um Fornecedor.

Banco de dados Utilizado:
 - MySQL